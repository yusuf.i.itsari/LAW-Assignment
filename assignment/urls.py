"""assignment URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
import oauth_example.urls as oauth_example
import tugas1.urls as tugas1
import cots1.urls as cots1
import lab.urls as lab

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^oauth-example/', include(oauth_example, namespace='oauth-example')),
    url(r'^tugas1/', include(tugas1, namespace='tugas1')),
    url(r'^cots1/', include(cots1, namespace='cots1')),
    url(r'^lab/', include(lab, namespace='lab')),
]
