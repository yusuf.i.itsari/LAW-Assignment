# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class UsersOauth(models.Model):
	userId = models.AutoField(primary_key=True)
	username = models.CharField(max_length=32)
	client_id = models.CharField(max_length=32)
	client_secret = models.CharField(max_length=32)
	hash_data = models.CharField(max_length=100)
	token_access = models.CharField(max_length=32)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)