from decouple import config
from ..models import Users
from . import response as response_services

import requests
import json
import logging

logger = logging.getLogger(__name__)

def doLogin(request):
	username = request.POST.get('username')
	password = request.POST.get('password')
	grant_type = config('TUGAS1_GRANT_TYPE', default='password')
	client_id = config('TUGAS1_CLIENT_ID', default='no client id')
	client_secret = config('TUGAS1_CLIENT_SECRET', default='no client secret')

	logger.info('[LOGIN] username: ' + username + ' passwor: ' + password)

	payload = {
		'username': username,
		'password': password,
		'grant_type': grant_type,
		'client_id': client_id,
		'client_secret': client_secret
	}

	url = 'http://172.22.0.2/oauth/token'
	r = requests.post(url, data=payload)

	data = r.json()

	status = 200
	response = data

	return status, response

def getAllUsers(request):
	try:
		page = int(request.GET.get('page',1))
		limit = int(request.GET.get('limit',25))

		offset = (page-1)*limit
		data = Users.objects.all()[offset:offset+limit]
		data = data.values()

		status = 200
		response = response_services.success(data)
		response['page'] = page
		response['limit'] = limit
	except Exception as e:
		logger.warning('[GET USERS] page: ' + page + ' limit: ' + limit)
		status = 400
		response = response_services.wrongRequestParameter()


	response = json.dumps(response,default=str)

	return status, response

def upsertUser(request):
	displayName = request.POST.get('displayName','')
	if displayName == '':
		status = 400
		response = json.dumps(response_services.wrongRequestParameter())
	else:
		data = Users(displayName=displayName)
		data.save()
		data = objUserToJson(data)

		status = 200
		response = json.dumps(response_services.success(data),default=str)

	return status, response

def deleteUser(request):
	parameter = request.read().split('=')
	parameter_name = parameter[0]
	id_user = parameter[1]
	
	if id_user != 0 and parameter_name == "id":
		try:
			data = Users.objects.get(id=id_user)
			data.delete()
			status = 200
			response = response_services.success(list(data))
			response.pop('data','nothing')
			reponse = json.dumps(response)
		except Exception as e:
			print e
			status = 400
			response = json.dumps(response_services.dataNotValid())
	else:
		status = 400
		response = json.dumps(response_services.wrongRequestParameter())

	return status, response

def objUserToJson(data):
	json_data = {
		'id': data.userId,
		'displayName': data.displayName,
		'createdAt': data.created_at,
		'updatedAt': data.updated_at,
	}

	return json_data