# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from PIL import Image

import os

# Create your views here.
def index(request):
	response = {
		'title': 'thumbnail in django',
	}

	return render(request, 'index.html', response)

def thumbnail(request):
	filename = os.path.dirname(__file__)+'/media/img/500kb'
	thumbnailPath = filename + "-thumbnail.jpeg"

	size = 200, 200
	img = Image.open(filename+".jpg")
	img.thumbnail(size)
	img.save(filename + "-thumbnail.jpeg", "JPEG")
	
	return HttpResponse(open(thumbnailPath), content_type='image/jpeg')