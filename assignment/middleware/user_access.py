from django.http import JsonResponse

import requests
import json

class UserAccessMiddleware(object):
	def __init__(self, get_response):
		self.get_response = get_response

	def __call__(self,request):
		access_token = request.META.get('HTTP_ACCESS_TOKEN')

		url = 'http://172.22.0.2/oauth/resource'
		headers = {
			"Authorization": "Bearer " + access_token
		}

		r = requests.get(url,headers=headers)
		r = r.json()

		if r.get('access_token'):
			response = self.get_response(request)
			return response
		else:
			status = 401
			message_response = {
				'status': 'fail',
				'message': 'Unauthorized'
			}

			print request.META

			return JsonResponse(message_response,status=status)