from django.conf.urls import url
from .views import index,thumbnail
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^thumbnail/$', thumbnail, name='thumbnail'),
]
