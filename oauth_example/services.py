from decouple import config

import urllib
import requests

def get_gitlab_oauth_token(authorize_code):
	client_id = config('GITLAB_CLIENT_ID',default='no client id')
	client_secret = config('GITLAB_CLIENT_SECRET',default='no client secret')
	grant_type = 'authorization_code'
	redirect_uri = urllib.quote_plus('http://127.0.0.1:8000/oauth_example/oauth_callback')
	url = 'http://gitlab.com/oauth/token'

	r = requests.post(url, data = {'client_id': client_id, 'client_secret': client_secret, 'code':authorize_code, 'grant_type': grant_type, 'redirect_uri': redirect_uri}) 
	print r
	
	return r
