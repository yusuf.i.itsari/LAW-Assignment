def success(data):
	response = {
		'status': 'ok',
		'data': data
	}

	return response

def dataNotValid():
	response = {
		'status': 'fail',
		'message': 'data not valid'
	}

	return response

def wrongRequestParameter():
	response = {
		'status': 'fail',
		'message': 'wrong request parameter'
	}
	
	return response

def method_not_allowed():
	response = {
		'status': 'fail',
		'message': 'method not allowed'
	}

	return response