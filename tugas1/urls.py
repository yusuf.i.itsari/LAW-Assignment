from django.conf.urls import url
from .views import index,login,user,comment
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login/$', login, name='login'),
    url(r'^user/$', user, name='user'),
    url(r'^comment/$', comment, name='comment'),
    url(r'^comment/(?P<id_comment>\d+)/$', comment, name='comment'),
]
