from django.conf.urls import url
from .views import index,authorized,oauth_callback
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^authorized', authorized, name='authorized'),
    url(r'^oauth_callback', oauth_callback, name='oauth_callback'),
]
