# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from services.response import method_not_allowed

import services.user as user_services
import services.comment as comment_services
import logging

logger = logging.getLogger(__name__)

# Create your views here.

def index(request):
	response = service.response()

	return render(request, 'something.html', response)

@csrf_exempt
def login(request):
	if request.method == 'POST':
		status, response = user_services.doLogin(request)
	else:
		status = 400
		logger.warning('using method ' + request.method + ' not POST')
		response = method_not_allowed()

	return JsonResponse(response, safe=False, status=status)

@csrf_exempt
def user(request):
	if request.method == 'GET':
		status, response = user_services.getAllUsers(request)
	elif request.method == 'POST' or request.method == 'PUT':
		status, response = user_services.upsertUser(request)
	elif request.method == 'DELETE':
		status, response = user_services.deleteUser(request)

	return JsonResponse(response, safe=False, status=status)

@csrf_exempt
def comment(request,id_comment=0):
	if request.method == 'GET':
		if id_comment != 0:
			status, response = comment_services.getCommentbyId(request,id_comment)
		else:
			status, response = comment_services.getAllComments(request)
	elif request.method == 'POST' or request.method == 'PUT':
		status, response = comment_services.upsertComment(request)
	elif request.method == 'DELETE':
		status, response = comment_services.deleteComment(request)


	return JsonResponse(response, safe=False, status=status)