# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import UsersOauth

import os,binascii
import json
import base64
import hashlib
import hmac

# Create your views here.
@csrf_exempt
def index(request):
	print request.META
	# channel_secret = ... # Channel secret string
	# body = ... # Request body string
	# hash = hmac.new(channel_secret.encode('utf-8'),
	#     body.encode('utf-8'), hashlib.sha256).digest()
	# signature = base64.b64encode(hash)
	# Compare X-Line-Signature request header and the signature

@csrf_exempt
def register(request):
	if request.method == 'POST':
		username = request.POST.get("username",'')
		client_id = default=binascii.b2a_hex(os.urandom(15))
		client_secret = default=binascii.b2a_hex(os.urandom(15))
		data = UsersOauth(username=username, client_id=client_id, client_secret=client_secret)
		data.save()

		data = objUserToJson(data)
		response = json.dumps(data,default=str)

		return JsonResponse(response, safe=False)

@csrf_exempt
def login(request):
	if request.method == 'POST':
		client_id = request.POST.get("client_id",'')
		client_secret = request.POST.get("client_secret",'')
		data = UsersOauth.objects.get(client_id=client_id,client_secret=client_secret)
		if data:
			payload = client_id+client_secret
			hash = hmac.new(client_secret.encode('utf-8'),
			    payload.encode('utf-8'), hashlib.sha256).digest()
			signature = base64.b64encode(hash)
			token_access = default=binascii.b2a_hex(os.urandom(15))
			data.token_access = token_access
			data.hash_data = signature
			data.save()
			response = json.dumps(objUserToJson(data),default=str)

		return JsonResponse(response, safe=False)


def objUserToJson(data):
	json_data = {
		'id': data.userId,
		'username': data.username,
		'client_id': data.client_id,
		'client_secret': data.client_secret,
		'token_access': data.token_access,
		'X-service-signature': data.hash_data
	}

	return json_data