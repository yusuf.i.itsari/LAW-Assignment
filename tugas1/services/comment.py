from ..models import Comments
from . import response as response_services
from django.core import serializers

import json

def getAllComments(request):
	page = int(request.GET.get('page',1))
	limit = int(request.GET.get('limit',25))
	created_by = request.POST.get('created_by','')
	start_date = request.POST.get('start_date','')
	end_date = request.POST.get('end_date','')

	offset = (page-1)*limit
	data = Comments.objects.all()[offset:offset+limit]
	data = data.values()
	# data = objCommentToJson(data)
	status = 200
	response = response_services.success(data)
	response['page'] = page
	response['limit'] = limit

	response = json.dumps(response,default=str)

	return status, response

def getCommentbyId(request,id_comment):
	if id_comment != 0:
		try:
			data = Comments.objects.get(id=id_comment)
			data = objCommentToJson(data)
			status = 200
			response = json.dumps(response_services.success(data),default=str)
		except Exception as e:
			print e
			status = 400
			response = json.dumps(response_services.dataNotValid(),default=str)
	else:
		status = 400
		response = json.dumps(response_services.wrongRequestParameter(),default=str)

	return status, response

def upsertComment(request):
	id_comment = request.POST.get('id',0)
	created_by = request.session.get('username','tester')
	comment = request.POST.get('comment','')

	if id_comment == 0:
		data = Comments(comment=comment, created_by=created_by)
		data.save()
		status = 200
		data = objCommentToJson(data)
		response = json.dumps(response_services.success(data),default=str)

	else:
		try:
			data = Comments.objects.get(id=id_comment)
			data.comment = comment
			data.save()
			data = objCommentToJson(data)
			status = 200
			response = json.dumps(response_services.success(data),default=str)
		except Exception as e:
			print e
			status = 400
			response = json.dumps(response_services.dataNotValid(),default=str)

	return status, response

def deleteComment(request):
	parameter = request.read().split('=')
	parameter_name = parameter[0]
	id_comment = parameter[1]

	if id_comment != 0 and parameter_name == "id":
		try:
			data = Comments.objects.get(id=id_comment)
			data.delete()
			status = 200
			response = response_services.success('')
			response.pop('data','nothing')
			reponse = json.dumps(response)
		except Exception as e:
			print e
			status = 400
			response = json.dumps(response_services.dataNotValid())	
	else:
		status = 400
		response = json.dumps(response_services.wrongRequestParameter())

	return status, response

def objCommentToJson(data):
	json_data = {
		'id': data.id,
		'comment': data.comment,
		'created_by': data.created_by,
		'createdAt': data.created_at,
		'updatedAt': data.updated_at,
	}

	return json_data