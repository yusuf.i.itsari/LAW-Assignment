# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class OauthExampleConfig(AppConfig):
    name = 'oauth_example'
