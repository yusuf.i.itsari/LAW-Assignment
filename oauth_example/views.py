# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from decouple import config

import urllib
import services

# Create your views here.
def index(request):
	client_id = config('GITLAB_CLIENT_ID',default='no client id')
	redirect_uri = urllib.quote_plus('http://127.0.0.1:8000/oauth-example/authorized')
	response = {'content': 'Gitlab Oauth Example', 
				'client_id':client_id, 
				'redirect_uri':redirect_uri}
	
	return render(request, 'gitlab_oauth.html', response)

def authorized(request):
	authorize_code = request.GET.get('code','')
	data = services.get_gitlab_oauth_token(authorize_code)
	response = {'content': data}
	return render(request, 'gitlab_oauth.html', response)

def oauth_callback(request):

	return
	

# def gitlab_oauth(request):
# 	gl = gitlab.Gitlab('http://localhost', oauth_token=oauth_token)