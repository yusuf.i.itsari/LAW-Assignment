from django.conf.urls import url
from .views import index,register,login
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^register', register, name='register'),
    url(r'^login', login, name='login'),
]
